<?php

namespace App\Http\Request\Admin\Company;

use Illuminate\Foundation\Http\FormRequest;

class CreateCompaniesRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string','max:255'],
            'is_show_on_main_page'=>['sometimes','boolean'],
            'is_not_show_on_main_page'=>['sometimes','boolean'],

        ];
    }
    public function messages()
    {
        return [
            'name.max' => 'Превышено допустимое колличество символов',
            'description.max'=>'Превышено допустимое колличество символов '
        ];
    }
}
