<?php

namespace App\Http\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RegisterUserRequest extends FormRequest
{
    public function authorize()
    {

        //Аутентификация более высокого уровня (тут можно проверить по ролям и тд) выдаст 403 ошибку
        // Конкретно про Диму тут плохой пример, правильно сделать валидацией ниже
        //Получение
//        $name = $this->get('name');
//
//        if ($name === 'Дима') {
//            //Никаких Дим не регистрируем!!
//            return false;
//        }

        return true;
    }

    public function rules()
    {
        return [
            //Rule::notIn не принимать значения из массива значений.
            'name' => ['required', 'string', 'max:255',  Rule::notIn([ 'Дмитрий', 'Дмитрий Тимошенко']),],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'], //unique - название валидации. :users - параметр который был передан в метод unique
            'password' => ['required', 'string', 'min:8', 'confirmed'], //confirmed будет ожидать поле с постфиксом _confirmation
            'phone'=>['required','digits:12']
            //TODO добавить валидацию на телефон в определённом формате.
        ];
    }

    public function messages()
    {
        return [
            'email.unique' => 'Данный Email уже занят',
            'name.not_in' => 'Никаких Дим!',
            'phone.between'=>'Неверный формат '
        ];
    }
}
