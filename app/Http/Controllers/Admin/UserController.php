<?php

namespace App\Http\Controllers\Admin;

use App\Http\Request\Admin\User\CreateUserRequest;
use App\Http\Request\Admin\User\UpdateUserRequest;
use App\Models\Role;
use App\Service\UserService;
use App\Models\User;

class UserController extends CrudController
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * UserController constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

//    public function add()
//    {
//        return view($this->getTemplateFolderPath().'form');
//    }

    public function create(CreateUserRequest $request)
    {
        $this->userService->create($request);

        // РАБОТАЕТ ТОЛЬКО ДЛЯ ЛАРАВЕЛЬ - НЕПРАВИЛЬНЫЙ ПОДХОД
        // Правильный - Dependency Injection
        /* @var $userService UserService */
//        $userService = app(UserService::class);
//        $userService->create($request);

        return redirect()->route('users.index');
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        // Ларавель умеет находить модели через параметры в адресной строке.
//      $user = User::findOrFail($id);
        $this->userService->update($request, $user);

        return redirect()->route('users.index');
    }

    public function delete(User $user)
    {
        $this->userService->delete($user);

        return redirect()->route('users.index');
    }

    protected function getTemplateFolderPath()
    {
        return 'admin.users.';
    }

    protected function getEntityModel()
    {
        return User::class;
    }

    public function addFormViewData() {
        return [
            'roles' => Role::all(),
        ];
    }

    public function editFormViewData($entity) {

        //Одинаковый результат
//        $arrayIdRoles = $entity->roles->map(function($role) {
//            return $role->id;
//        })->toArray();
//
//        $arrayIdRoles = $entity->roles->pluck('id')->toArray();

        return [
            'roles' => Role::all(),
            'arrayIdRoles' => $entity->roles->pluck('id')->toArray(),
        ];
    }

}
