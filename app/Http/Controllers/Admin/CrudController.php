<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use Illuminate\Routing\Controller as BaseController;
use phpDocumentor\Reflection\Types\Array_;

abstract class CrudController extends BaseController
{
    public function add()
    {
        return view($this->getTemplateFolderPath().'add', $this->addFormViewData());
    }

    public function index()
    {
        $entities = ($this->getEntityModel())::paginate(10);
        return view($this->getTemplateFolderPath().'.index', compact('entities'));
    }

    public function edit($id)
    {
        $entity = ($this->getEntityModel())::findOrFail($id);

        $entityViewData = ['entity' => $entity,];

        return view($this->getTemplateFolderPath() . '.edit',
            array_merge($entityViewData, $this->editFormViewData($entity)));
    }


    protected function getTemplateFolderPath()
    {
        return '';
    }

    protected function getEntityModel()
    {
        return '';
    }

    public function addFormViewData() {
        return [];
    }

    public function editFormViewData($entity) {
        return [];
    }
}
