<?php

namespace App\Http\Controllers\Admin;

use App\Models\CompaniesType;
use App\Models\Company;
use App\Http\Controllers\Admin\CrudController;
use App\Http\Request\Admin\CompanyRequest;
use App\Http\Request\Admin\Company\CreateCompaniesRequest;
use App\Http\Request\Admin\Company\UpdateCompaniesRequest;
use App\Service\CompanyService;
use Illuminate\Http\Request;

class CompanyController extends CrudController
{
    /**
     * @var CompanyService
     */
    private $companyService;


    public function __construct(CompanyService $companyService)
    {
        $this->companyService = $companyService;
    }

    public function create(CreateCompaniesRequest $request)
    {
       $this->companyService->create($request);

        //!!! ТАК НИ В КОЕМСЛУЧАЕ ДЕЛАТЬ НЕЛЬЗЯ
//        $Company->forceFill($request->all());
        /* @var $companyService CompanyService */
        //$Company->forceFill($request->only([
       //     'name',
      //      'description',
       // ]));

//        $Company->forceFill([
//            'name' => $request->get('name'),
//            'description' => $request->get('description'),
//        ]);
//
//        $Company->name = $request->get('name');
//        $Company->description = $request->get('description');

        //$Company->save();


        // Redirect - переадресация @learnEnglish
//        <a href="asdsadasd">Релятивная дописывающая</a>
//        <a href="/asdsadasd">Релятивная от корня сайта потому что есть слеш в начале</a>
//        <a href="https://asdsadsad.com/asdsadasd">Абсолютная (пропиан полный путь)</a>

        // Редирект на конкретный url
        // Внешний (абсолютный)
        // return redirect('https://google.com');
        // Внутренний (релятивный)
        // return redirect('/companies/'.$Company->id);
        // Редирект по названию route
        // На конкретную форму
//        return redirect()->route('companies.edit', ['id' => $Company->id]);
        // На список
        return redirect()->route('admin.companies.index');
    }



    public function update(UpdateCompaniesRequest $request, Company $company)
    {
        $this->companyService->update($request,$company);
        return redirect()->route('admin.companies.index');
    }

    public function delete(Company $company)
    {
        $this->companyService->delete($company);
        return redirect()->route('admin.companies.index');
    }

    protected function getEntityModel()
    {
        return Company::class;
    }

    protected function getTemplateFolderPath()
    {
        return 'admin.companies.';
    }

    public function addFormViewData() {
        return [
            'types' => CompaniesType::all(),
        ];
    }
    public function editFormViewData($entity)
    {
        return [
            'types'=>CompaniesType::all(),
            'idTypeCompany'=>$entity->company_type_id,
        ];
    }
}
