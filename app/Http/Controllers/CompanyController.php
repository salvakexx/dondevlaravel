<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show(Request $request, Company $company)
    {
//        Достать параметр из url по его названию например /company/{companyId}
//        request()->route('companyId')

        return view('pages.company', ['company' => $company]);
    }
}
