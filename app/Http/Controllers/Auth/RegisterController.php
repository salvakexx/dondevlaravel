<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Request\RegisterUserRequest;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\JsonResponse;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Куда пользователя переадресовать после регистрации
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = new User();

        //функция fill - будет использовать только те поля, которые объявлены в поле $fillable из модели User
        $user->fill([
            'name' => $data['name'],
        ]);
        //функция forceFill -игнорирует $fillable из модели User
        $user->forceFill([
            'email' => $data['email'],
            'phone' => $data['phone'],
        ]);

        //Явно задаем значение поля password
        //Так можно в laravel
        //$user->password = Hash::make($data['password']);
        //А так правильно для любого фреймворка
        $user->setPassword($data['password']);

        $user->save();

        return $user;
//      Более быстрый вариант но не явный, и мы завязаны на поле $fillable из модели User
//        return User::create([
//            'name' => $data['name'],
//            'email' => $data['email'],
//            'password' => Hash::make($data['password']),
//            'phone' => $data['phone'],
//        ]);
    }

    /*
     *  Переопределим метод register для того.
     */
    public function register(RegisterUserRequest $request)
    {
        $inputData = $request->all();

        $user = $this->create($inputData);

        event(new Registered($user));

        $this->guard()->login($user);

        if ($response = $this->registered($request, $user)) {
            return $response;
        }

        return $request->wantsJson()
            ? new JsonResponse([], 201)
            : redirect($this->redirectPath());
    }
}
