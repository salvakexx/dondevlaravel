<?php

namespace App\Console\Commands;

use App\Models\Role;
use App\Models\User;
use Illuminate\Console\Command;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:create
                            {email : Email of created admin}
                            {password : Password of created admin} 
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates the admin user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $user = new User();

        $password = $this->argument('password');

        $user->forceFill(
            [
                'name' => 'Admin',
                'email' => $this->argument('email'),
            ]
        )
            ->setPassword($password)
            ->save()
        ;

        $adminRole = Role::where('key', '=', 'admin')->first();
        $userRole = Role::where('key', '=', 'user')->first();

        $user->roles()->sync([$adminRole->id, $userRole->id]);

        $this->info('Generated user with id='.$user->id);
    }
}
