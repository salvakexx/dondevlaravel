<?php
namespace App\Service;

use App\Http\Controllers\Admin\UserController;
use App\Models\User;
use Illuminate\Http\Request;

class UserService
{
    public function create(Request $request)
    {
        $user = new User();

        $user->forceFill($request->only(['name', 'email', 'phone']))
            ->setPassword($request->get('password'))
            ->save()
        ;

        $user->roles()->sync($request->get('roles'));

        return $user;
    }

    public function update(Request $request, User $user)
    {
        $user->forceFill($request->only(['name', 'email', 'phone']));

        if ($request->get('password')) {
            $user->setPassword($request->get('password'));
        }

        $user->save();
        $user->roles()->sync($request->get('roles'));
        return $user;
    }


    public function delete(User $user)
    {
        $user->delete();
    }
}
