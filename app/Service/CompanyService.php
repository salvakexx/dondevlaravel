<?php
namespace App\Service;

use App\Models\CompaniesType;
use App\Models\Company;
use Illuminate\Http\Request;
use function GuzzleHttp\Promise\all;

class CompanyService
{
    public function create(Request $request)
    {
        $company = new Company();
        $company->forceFill($request->only([
            'name',
            'description',
            'company_type_id',
        ]));

        $company->is_show_on_main_page = $request->has('is_show_on_main_page');

        $company->save();
        return $company;
    }

    public function update(Request $request, Company $company)
    {
        $company->forceFill($request->only([
            'name',
            'description',
            'company_type_id'
        ]));

        $company->is_show_on_main_page = $request->has('is_show_on_main_page');

        $company->save();
        return $company;
    }

    public function delete(Company $company)
    {
        $company->delete();
    }
}
