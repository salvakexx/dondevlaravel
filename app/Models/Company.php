<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{

    public function types()
    {
        return $this->belongsTo('App\Models\CompaniesType','company_type_id');
    }

}
