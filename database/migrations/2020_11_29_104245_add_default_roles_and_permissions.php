<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDefaultRolesAndPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('roles')->insert([
            'name' => 'Admin',
            'key' => 'admin'
        ]);

        DB::table('roles')->insert([
            'name' => 'User',
            'key' => 'user'
        ]);

        DB::table('permissions')->insert([
            'name' => 'Manage Users',
            'key' => 'manage-users'
        ]);

        DB::table('permissions')->insert([
            'name' => 'Manage Companies',
            'key' => 'manage-companies'
        ]);

        DB::table('permissions')->insert([
            'name' => 'Admin access',
            'key' => 'admin'
        ]);

        DB::table('permissions')->insert([
            'name' => 'Login access',
            'key' => 'login'
        ]);

        $adminRole = DB::table('roles')->where('key', '=', 'admin')->first();

        $manageUsersPermission = DB::table('permissions')->where('key', '=', 'manage-users')->first();
        $adminAccessPermission = DB::table('permissions')->where('key', '=', 'admin')->first();
        $loginPermission = DB::table('permissions')->where('key', '=', 'login')->first();

        DB::table('roles_permissions')->insert([
            'role_id' => $adminRole->id,
            'permission_id' => $manageUsersPermission->id,
        ]);

        DB::table('roles_permissions')->insert([
            'role_id' => $adminRole->id,
            'permission_id' => $adminAccessPermission->id,
        ]);

        $userRole = DB::table('roles')->where('key', '=', 'user')->first();

        DB::table('roles_permissions')->insert([
            'role_id' => $userRole->id,
            'permission_id' => $loginPermission->id,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('roles')->whereIn('key', [
            'admin',
            'user',
        ])->delete();

        DB::table('permissions')->whereIn('key', [
            'manage-users',
            'manage-companies',
            'admin',
            'login',
        ])->delete();
    }
}
