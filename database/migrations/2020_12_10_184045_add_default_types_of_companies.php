<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDefaultTypesOfCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('companies_types')->insert([
            'name' => 'Коммерческая',
        ]);
        DB::table('companies_types')->insert([
            'name' => 'Строительная',
        ]);
        DB::table('companies_types')->insert([
            'name' => 'Производственная',
        ]);
        DB::table('companies_types')->insert([
            'name' => 'Финансовая',
        ]);
        DB::table('companies_types')->insert([
            'name' => 'Консультативная',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('companies_types')->whereIn('name', [
            'Коммерческая',
            'Строительная',
            'Производственная',
            'Финансовая',
            'Консультативная',
        ])->delete();
    }
}
