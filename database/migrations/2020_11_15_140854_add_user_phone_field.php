<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserPhoneField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::table( - метод для изменения существующей таблицы
        Schema::table('users', function (Blueprint $table) {
            // ->nullable(); - указывает на то что поле может быть пустым.
           $table->string('phone')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //dropColumn - удалить
            $table->dropColumn('phone');
        });
    }
}
