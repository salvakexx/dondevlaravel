<?php
use Illuminate\Support\Facades\Route;
Route::middleware('auth')->group(function() {

    Route::prefix('companies')->name('companies.')->group(function() {
        Route::get('/', 'CompanyController@index')->name('index');
        Route::get('/add', 'CompanyController@add')->name('add');
        Route::get('/{id}','CompanyController@edit' )->name('edit');
        Route::get('/delete/{company}','CompanyController@delete' )->name('delete');
        Route::post('/create', 'CompanyController@create')->name('create');
        Route::post('/{company}','CompanyController@update' )->name('update');
    });

    Route::prefix('users')->name('users.')->group(function() {
        Route::get('/add', 'UserController@add')->name('add');
        Route::post('/create', 'UserController@create')->name('create');
        Route::get('/', 'UserController@index')->name('index');
        Route::get('/{id}','UserController@edit' )->name('edit');
        Route::post('/{user}','UserController@update' )->name('update');
        Route::get('/delete/{user}','UserController@delete' )->name('delete');
    });

});
