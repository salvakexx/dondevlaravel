<?php

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/contacts', function(){
    return view('pages.contacts');
})->name('contacts');

Route::get('/company/{company}', 'CompanyController@show')->name('company');
