<div class="form-group">
    <label for="{{$fieldName}}-id">
        {{$fieldLabel ?? $fieldName}}
    </label>
    <select @if($multiple === true) multiple @endif class="form-control mb-3 @error($fieldName) is-invalid @enderror" name="{{$fieldName}}@if($multiple === true)[]@endif" id="{{$fieldName}}-id">
        @foreach($options as $option)

            <option value="{{$option['value']}}" @if( in_array($option['value'], $selectedValues ?? [])) selected @endif>{{$option['label']}}</option>
        @endforeach
    </select>
    @error($fieldName)
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>
