<div class="form-group">
    <label for="{{$fieldName}}">
        {{__('users.'.$fieldName)}}
    </label>
    <input class="form-control mb-3 @error($fieldName) is-invalid @enderror" type="password" name="{{$fieldName}}"  placeholder="{{__('users.'.$fieldName)}}" value="{{ $value ?? old($fieldName) }}" id="{{$fieldName}}">
    @error($fieldName)
    <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>
