<div class="form-check-inline">
    <label class="form-check-label ml-5"for="{{$fieldName}}">
        {{__('companies.'.$fieldName)}}
    </label>
    <input class="form-check-input @error($fieldName) is-invalid @enderror" type="checkbox" name="{{$fieldName}}"  placeholder="{{__('companies.'.$fieldName)}}" value="1" id="{{$fieldName}}">
    @error($fieldName)
    <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>
