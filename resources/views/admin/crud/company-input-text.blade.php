<div class="form-group">
    <label for="{{$fieldName}}">
        {{__('companies.'.$fieldName)}}
    </label>
    <input class="form-control mb-3 @error($fieldName) is-invalid @enderror" type="text" name="{{$fieldName}}"  placeholder="{{__('companies.'.$fieldName)}}" value="{{ $value ?? old($fieldName) }}" id="{{$fieldName}}">
    @error($fieldName)
    <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>
