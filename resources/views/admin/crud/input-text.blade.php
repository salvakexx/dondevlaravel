<div class="form-group">
    <label for="{{$fieldName}}">
        {{$fieldLabel ?? $fieldName}}
    </label>
    <input class="form-control mb-3 @error($fieldName) is-invalid @enderror" @if($fieldName=='password')type="password"@else type="text" @endif name="{{$fieldName}}"  placeholder="{{$fieldLabel ?? $fieldName}}" value="{{ $value ?? old($fieldName) }}" id="{{$fieldName}}">
    @error($fieldName)
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>
