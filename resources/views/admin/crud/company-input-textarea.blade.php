<div class="form-group">
    <label for="{{$fieldName}}">
        {{__('companies.'.$fieldName)}}
    </label>
    <textarea class="form-control  @error($fieldName) is-invalid @enderror " name="{{$fieldName}}"  placeholder="{{__('companies.'.$fieldName)}}" value="{{ $value ?? old($fieldName) }}" id="{{$fieldName}}" cols="15" rows="10"></textarea>
    @error($fieldName)
    <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>
