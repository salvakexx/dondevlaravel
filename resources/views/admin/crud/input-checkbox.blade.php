<div class="form-check-inline">
    <label class="form-check-label ml-5"for="{{$fieldName}}">
        {{$fieldLabel ?? $fieldName}}
        <input class="form-check-input @error($fieldName) is-invalid @enderror" type="checkbox" name="{{$fieldName}}" @if(isset($value) && $value ? true : false) checked @endif value="1" id="{{$fieldName}}">
    </label>

    @error($fieldName)
    <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>
