<div class="form-group">
    <label for="{{$fieldName}}">
        {{$fieldLabel ?? $fieldName}}
    </label>
    <textarea class="form-control  @error($fieldName) is-invalid @enderror " name="{{$fieldName}}"  placeholder="{{$fieldLabel ?? $fieldName}}" id="{{$fieldName}}" cols="15" rows="10">{{ $value ?? old($fieldName) }}</textarea>
    @error($fieldName)
    <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>
