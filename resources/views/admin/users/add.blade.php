@extends('admin.layout')
@section('title','Добавить пользователя')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Добавить пользователя</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('admin.users.create') }}" method="post">
                    @csrf

                    @include('admin.crud.input-text', ['fieldName' => 'name', 'fieldLabel' => __('users.name')])
                    @include('admin.crud.input-text', ['fieldName' => 'email'])
                    @include('admin.crud.input-text', ['fieldName' => 'phone'])

                    <div class="form-group">
                        <label for="name">{{__('users.password')}}</label>
                        <input class="form-control mb-3" type="password" name="password" placeholder="{{__('users.password')}}" value="" id="password">
                    </div>

                    @include('admin.crud.input-select', ['fieldName' => 'roles', 'multiple' => true, 'options' =>
                        $roles->map(function($role) {
                            return [
                               'value' => $role->id,
                               'label' => $role->name
                            ];
                        })
                    ])

                    <div class="form-group">
                        <button type="submit" class="btn btn-secondary mt-2">{{__('crud.save')}}</button>
                    </div>
                </form>
            </div>
        </div>

@endsection
