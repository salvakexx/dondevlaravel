@extends('admin.layout')
@section('title','Добавить компанию в список')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Изменить пользователя</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('admin.users.update', ['user' => $entity->id]) }}" method="post">
                    @csrf
                    @include('admin.crud.input-text', ['fieldName' => 'name', 'value' => $entity->name])
                    @include('admin.crud.input-text', ['fieldName' => 'email', 'value' => $entity->email])
                    @include('admin.crud.input-text', ['fieldName' => 'phone', 'value' => $entity->phone])
                    @include('admin.crud.input-password', ['fieldName' => 'password', 'value' => null])

                    @include('admin.crud.input-select', ['fieldName' => '', 'multiple' => true, 'options' =>
                        $roles->map(function($role) {
                            return [
                               'value' => $role->id,
                               'label' => $role->name
                            ];
                        }),
                        'selectedValues' => $arrayIdRoles,
                        'fieldLabel' => __('users.roles')
                    ])

                    <button type="submit" class="btn btn-secondary mt-2">Изменить</button>
                </form>
            </div>
        </div>

@endsection
