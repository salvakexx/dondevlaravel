@extends('admin.layout')

@section('content')

    <div class="container">
        <a class="btn btn-info" href="{{route('admin.users.add')}}">{{__('crud.index_add', ['entityName' => __('users.entityName')])}}</a>
        <h2 class="text-center">{{__('crud.index_list', ['entityName' => __('users.entityName')])}}:</h2>
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">№</th>
                <th scope="col">{{__('users.name')}}</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @if(isset($entities))
                @foreach($entities as $entity)
                    <tr>
                        <td scope="row">
                            {{$entity->id}}
                        </td>
                        <td>
                            {{$entity->name}}
                        </td>
                        <td>
                            <a href="{{route('admin.users.edit', ['id' => $entity->id])}}" class=" btn btn-warning">Редактировать</a>
                            <a href="{{route('admin.users.delete', ['user' => $entity->id])}}" class=" btn btn-danger"> Удалить</a>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
    @if($entities->total() > $entities->count())
        <div class="row  mx-auto" style="width: 200px;">
            <div class="col-md-12">
                <p class="text-center">{{ $users->links() }}</p>
            </div>
        </div>
    @endif
@endsection
