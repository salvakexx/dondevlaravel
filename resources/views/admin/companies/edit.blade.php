@extends('layouts.app')
@section('title','Добавить компанию в список')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Изменить информацию компании</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('admin.companies.update', ['company' => $entity->id]) }}" method="post">
                    @csrf
                    @include('admin.crud.company-input-text', ['fieldName' => 'name', 'value' => $entity->name])
                    @include('admin.crud.input-select',  [
                            'fieldName' => 'company_type_id',
                            'multiple' => false,
                            'options' => $types->map(function($type) {
                                                              return [
                                                                   'value' => $type->id,
                                                                   'label' => $type->name
                                                               ];
                                                      }),
                            'fieldLabel' => __('companies.company_type_id'),
                            'selectedValues' => [ $idTypeCompany ]
                    ])
                    @include('admin.crud.input-textarea', ['fieldName' => 'description', 'fieldLabel' => __('companies.description'), 'value' => $entity->description])
                    @include('admin.crud.input-checkbox', ['fieldName'=>'is_show_on_main_page', 'fieldLabel' => __('companies.is_show_on_main_page'), 'value' => $entity->is_show_on_main_page])
                    <button type="submit" class="btn btn-secondary mt-2">{{__('crud.change')}}</button>
                </form>
            </div>
        </div>

@endsection
