@extends('layouts.app')
@section('title','Добавить компанию в список')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Добавить компанию в список</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
{{--                <form action="{{ url('companies/create') }}" method="post">--}}
                <form action="{{ route('admin.companies.create') }}" method="post">
                    @csrf
                    @include('admin.crud.company-input-text', ['fieldName' => 'name'])
                    @include('admin.crud.input-select',  [
                        'fieldName' => 'company_type_id',
                        'multiple' => false,
                        'options' => $types->map(function($type) {
                                                          return [
                                                               'value' => $type->id,
                                                               'label' => $type->name
                                                           ];
                                                  }),
                        'fieldLabel' => __('companies.company_type_id')
                    ])
                    @include('admin.crud.input-textarea', ['fieldName' => 'description', 'fieldLabel' => __('companies.description')])
                    @include('admin.crud.input-checkbox',['fieldName'=>'is_show_on_main_page', 'fieldLabel' => __('companies.is_show_on_main_page')])
                    <button type="submit" class="btn btn-secondary mt-2">{{__('crud.save')}}</button>
                </form>
            </div>
        </div>

@endsection
