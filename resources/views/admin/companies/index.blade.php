@extends('layouts.app')

@section('content')



    <div class="container">
        <div class="row">
            <div class="col-12">
                <a class="btn btn-info" href="{{route('admin.companies.add')}}">{{__('crud.index_add', ['entityName' => __('companies.entityName')])}}</a>
            </div>
            <div class="col-12">
                <h2 class="text-center">{{__('crud.index_list', ['entityName' => __('companies.entityName')])}}:</h2>
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">{{__('companies.number')}}</th>
                        <th scope="col">{{__('companies.name')}}</th>
                        <th scope="col">{{__('companies.description')}}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($entities))
                        @foreach($entities as $company)
                            <tr>
                                <td scope="row">{{$company->id}}</td>
                                <td>{{$company->name}}</td>
                                <td>{{$company->description}}</td>
                                <td><a href="{{route('admin.companies.edit',['id' => $company->id])}}" class=" btn btn-warning"> {{__('crud.edit')}} </a>
                                    <a href="{{route('admin.companies.delete',$company->id)}}" class=" btn btn-danger "> {{__('crud.delete')}}</a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            </div>
        </div>


    @if($entities->total() > $entities->count())
        <div class="row  mx-auto" style="width: 200px;">
            <div class="col-md-12">
                <p class="text-center">{{ $entities->links() }}</p>
            </div>
        </div>
    @endif
@endsection
