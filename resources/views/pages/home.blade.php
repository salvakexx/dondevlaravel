@extends('layouts.app')

@section('content')
<div class="container">
    <h2 class="text-center">Главная страница</h2>
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">{{__('companies.number')}}</th>
                <th scope="col">{{__('companies.name')}}</th>
                <th scope="col">{{__('companies.description')}}</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @if(isset($companies ))
            @foreach($companies as $company)
                <tr>
                    <td scope="row">{{$company->id}}</td>
                    <td>{{$company->name}}</td>
                    <td>{{$company->description}}</td>
                    <td><a href="{{route('admin.companies.edit',['id' => $company->id])}}" class=" btn btn-warning"> {{__('crud.edit')}} </a>
                        <a href="{{route('admin.companies.delete',$company->id)}}" class=" btn btn-danger "> {{__('crud.delete')}}</a>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>



<!-- Heading Row -->
<div class="row align-items-center my-5">
    <div class="col-lg-7">
        <img class="img-fluid rounded mb-4 mb-lg-0" src="http://placehold.it/900x400" alt="">
    </div>
    <!-- /.col-lg-8 -->
    <div class="col-lg-5">
        <h1 class="font-weight-light">Business Name or Tagline</h1>
        <p>This is a template that is great for small businesses. It doesn't have too much fancy flare to it, but it makes a great use of the standard Bootstrap core components. Feel free to use this template for any project you want!</p>
        <a class="btn btn-primary" href="#">Call to Action!</a>
    </div>
    <!-- /.col-md-4 -->
</div>
<!-- /.row -->

<!-- Call to Action Well -->
<div class="card text-white bg-secondary my-5 py-4 text-center">
    <div class="card-body">
        <p class="text-white m-0">This call to action card is a great place to showcase some important information or display a clever tagline!</p>
    </div>
</div>

<!-- Content Row -->
<div class="row">
    @foreach($companies as $company)
        <div class="col-md-4 mb-5">
            <div class="card h-100">
                <div class="card-body">
                    <h2 class="card-title">{{$company->name}}</h2>
                    <p class="card-text">{{$company->description}}</p>
                </div>
                <div class="card-footer">
                    <a href="{{route('company', ['company' => $company->id])}}" class="btn btn-primary btn-sm">Подробнее</a>
                </div>
            </div>
        </div>
    @endforeach
</div>
<!-- /.row -->
@endsection
