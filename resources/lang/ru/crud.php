<?php

return [
    'index_add' => 'Добавить :entityName',
    'index_list' => 'Список :entityName',
    'edit' => 'Редактировать',
    'delete' => 'Удалить',
    'change' => 'Изменить',
    'save' => 'Сохранить',
];
