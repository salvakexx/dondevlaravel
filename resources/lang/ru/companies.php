<?php
return [
    'name'=> 'Название компании',
    'description'=>'Описание деятельности',
    'entityName' => 'Компаний',
    'company_type_id'=>'Тип компании',
    'number' => '№',
    'is_show_on_main_page'=>'Отображать на главной странице',
    'is_not_show_on_main_page'=>'Не отображать на главной странице',
];
